#!/usr/bin/python
import sys
import numpy as np
from sfml import sf


class GameOfLifeBoard(object):
    def __init__(self, width, height):
        if(not isinstance(width, int)):
            sys.exit("Error in GameOfLife: width is not integer!")
        if(not isinstance(height, int)):
            sys.exit("Error in GameOfLife: height is not integer!")
        self.width = width
        self.height = height
        self.field = np.full((height+2, width+2), 0)

    def set_cell(self, i, j, value):
        """
        Get cell with coordinates i and j.
        """
        if(not isinstance(i, int) and not isinstance(j, int)):
            sys.exit("Error in GameOfLife set_cell: coordinates are not integer!")
        if((i < 1 or i > self.height) or (j < 1 or j > self.width)):
            sys.exit("Error in GameOfLife set_cell: coordinates are outside!")
        if(value != 0 and value != 1):
            sys.exit("Error in GameOfLife set_cell: value is not 0 or 1!")
        self.field[i][j] = value

    def get_cell(self, i, j):
        """
        Set live cell with coordinates i and j.
        """
        if(not isinstance(i, int) and not isinstance(j, int)):
            sys.exit("Error in GameOfLife set_cell: coordinates are not integer!")
        if((i < 1 or i > self.height) or (j < 1 or j > self.width)):
            sys.exit("Error in GameOfLife set_cell: coordinates are outside!")
        return(self.field[i][j])

    def set_glider(self, i, j):
        """
        Set figure glider at coordinates i and j (left top of figure).
        """
        if(not isinstance(i, int) and not isinstance(j, int)):
            sys.exit("Error in GameOfLife set_glider: coordinates are not integer!")
        if((i < 1 or i > self.height-2) or (j < 1 or j > self.width-2)):
            sys.exit("Error in GameOfLife set_glider: coordinates are outside!")
        self.set_cell(0 + i, 1 + j, 1)
        self.set_cell(1 + i, 2 + j, 1)
        self.set_cell(2 + i, 0 + j, 1)
        self.set_cell(2 + i, 1 + j, 1)
        self.set_cell(2 + i, 2 + j, 1)

    def set_gosper_gun(self, i, j):
        """
        Set figure Gosper's Gun at coordinates i and j (left top of figure).
        """
        if(not isinstance(i, int) and not isinstance(j, int)):
            sys.exit("Error in GameOfLife set_glider: coordinates are not integer!")
        if((i < 1 or i > self.height-10) or (j < 1 or j > self.width-37)):
            sys.exit("Error in GameOfLife set_glider: coordinates are outside!")
        self.set_cell(6 + i, 2 + j, 1)
        self.set_cell(7 + i, 2 + j, 1)
        self.set_cell(6 + i, 3 + j, 1)
        self.set_cell(7 + i, 3 + j, 1)

        self.set_cell(6 + i, 12 + j, 1)
        self.set_cell(7 + i, 12 + j, 1)
        self.set_cell(8 + i, 12 + j, 1)
        self.set_cell(5 + i, 13 + j, 1)
        self.set_cell(9 + i, 13 + j, 1)
        self.set_cell(4 + i, 14 + j, 1)
        self.set_cell(10 + i, 14 + j, 1)
        self.set_cell(4 + i, 15 + j, 1)
        self.set_cell(10 + i, 15 + j, 1)
        self.set_cell(7 + i, 16 + j, 1)
        self.set_cell(9 + i, 17 + j, 1)
        self.set_cell(5 + i, 17 + j, 1)
        self.set_cell(8 + i, 18 + j, 1)
        self.set_cell(7 + i, 18 + j, 1)
        self.set_cell(6 + i, 18 + j, 1)
        self.set_cell(7 + i, 19 + j, 1)

        self.set_cell(4 + i, 22 + j, 1)
        self.set_cell(5 + i, 22 + j, 1)
        self.set_cell(6 + i, 22 + j, 1)
        self.set_cell(4 + i, 23 + j, 1)
        self.set_cell(5 + i, 23 + j, 1)
        self.set_cell(6 + i, 23 + j, 1)
        self.set_cell(3 + i, 24 + j, 1)
        self.set_cell(7 + i, 24 + j, 1)
        self.set_cell(3 + i, 26 + j, 1)
        self.set_cell(7 + i, 26 + j, 1)
        self.set_cell(2 + i, 26 + j, 1)
        self.set_cell(8 + i, 26 + j, 1)

        self.set_cell(4 + i, 36 + j, 1)
        self.set_cell(5 + i, 36 + j, 1)
        self.set_cell(4 + i, 37 + j, 1)
        self.set_cell(5 + i, 37 + j, 1)

    def count_neighbors(self, i, j):
        """
        Count and return number of neighbors cell with coordinates i and j.
        """
        count = self.field[i][j-1] + self.field[i][j+1]
        count += self.field[i+1][j-1] + \
            self.field[i+1][j] + self.field[i+1][j+1]
        count += self.field[i-1][j-1] + \
            self.field[i-1][j] + self.field[i-1][j+1]
        return(count)

    def do_step(self):
        """
        Get the next generation.
        """
        new_field = np.copy(self.field) # (:)
        for i in range(1, self.height+1):
            for j in range(1, self.width+1):
                n = self.count_neighbors(i, j)
                if(self.field[i][j] == 0 and n == 3):
                    new_field[i][j] = 1
                if(self.field[i][j] == 1 and n != 2 and n != 3):
                    new_field[i][j] = 0
        self.field = np.copy(new_field)


class GameOfLifeWindow(object):
    def __init__(self, GameBoard, cell_size, title, FPS):
        if(not isinstance(cell_size, int)):
            sys.exit("Error in Game: cell_size is not integer!")
        self.cell_size = cell_size
        self.GameBoard = GameBoard

        W = GameBoard.width * cell_size
        H = GameBoard.height * cell_size
        self.window = sf.RenderWindow(sf.VideoMode(W, H), title)
        self.window.framerate_limit = FPS

        self.cell = sf.RectangleShape(sf.Vector2(cell_size, cell_size))
        self.cell.fill_color = sf.Color.BLACK

    def loop(self):
        while self.window.is_open:
            for event in self.window.events:
                if event == sf.Event.CLOSED:
                    self.window.close()

            self.window.clear(sf.Color.WHITE)
            for i in range(1, self.GameBoard.height+1):
                for j in range(1, self.GameBoard.width+1):
                    if(self.GameBoard.get_cell(i, j) == 1):
                        self.cell.position = (
                            (j - 1) * self.cell_size, (i - 1) * self.cell_size)
                        self.window.draw(self.cell)
            self.window.display()
            self.GameBoard.do_step()


def main():
    GameBoard = GameOfLifeBoard(width=60, height=50)
    #GameBoard.set_glider(1,1)
    GameBoard.set_gosper_gun(1, 1)
    game = GameOfLifeWindow(GameBoard, cell_size=10, title="Game of Life", FPS=15)
    game.loop()


if __name__ == '__main__':
    main()
