#!/usr/bin/python
import os
import sys
import argparse
import urllib.request
from time import sleep
from math import copysign
from multiprocessing import Pool


def convert_coordinates(clock_coord):
    """
    Convert coordinates from (hours/degrees-min-sec) to (degrees) and return them.
    """
    deg_coord = clock_coord
    RA = clock_coord[0].split(':')
    deg_coord[0] = 15 * float(RA[0]) + float(RA[1]) / 4 + float(RA[2]) / 240

    DEC = clock_coord[1].split(':')
    deg_coord[1] = float(DEC[0])
    deg_coord[1] += copysign(1, float(DEC[0])) * (float(DEC[1]) / 60 + float(DEC[2]) / 3600)
    return(deg_coord)


class Galaxy(object):
    def __init__(self, data_list, galaxy_type):
        self.ID = str(data_list[0])
        self.type = galaxy_type
        coord = convert_coordinates(data_list[1:3])
        self.RA = coord[0]
        self.DEC = coord[1]

    def show_information(self):
        print(f"ID: {self.ID}")
        print(f"Type: {self.type}")
        print(f"RA: {self.RA}")
        print(f"DEC: {self.DEC}")

    def download_image(self):
        """
        Download image of sky from SDSS. Image (.jpg) name is galaxy ID.
        Return code of success (0) or code of fail (1).
        """
        image_name = self.ID + ".jpg"
        URL = "http://skyserver.sdss.org/dr14/SkyServerWS/ImgCutout/"
        URL += "getjpeg?TaskName=Skyserver.Explore.Image"
        URL += "&ra=" + str(self.RA) + "&dec=" + str(self.DEC)
        URL += "&scale=0.2&width=200&height=200&opt=G"
        try:
            urllib.request.urlretrieve(URL, image_name)
            return(0)
        except urllib.error.HTTPError:
            print("Doesn't exist images of this part of sky. Download is missed.")
            return(1)


def read_galaxies_data(file_name):
    """
    Read Galaxy Zoo's data from file_name and return galaxies data (list of lists).
    """
    with open(file_name, "r") as galaxies_file:
        data = galaxies_file.readlines()

    galaxy_data_list = []
    for line in data:
        line_data = line.strip().split(',')
        galaxy_data_list.append(line_data)
    return(galaxy_data_list[1:])


def get_spiral_galaxies(galaxy_data_list, criteria):
    """
    Find (according criteria) and return spiral galaxies (list of objects class Galaxy) from galaxies data.
    """
    spiral_galaxy_list = []
    for galaxy_data in galaxy_data_list:
        if (float(galaxy_data[5]) > criteria or float(galaxy_data[6]) > criteria):
            spiral_galaxy_list.append(Galaxy(galaxy_data[0:3], "Spiral"))
    return(spiral_galaxy_list)


def get_ellipt_galaxies(galaxy_data_list, criteria):
    """
    Find (according criteria) and return ellipt galaxies (list of objects class Galaxy) from galaxies data.
    """
    spiral_galaxy_list = []
    for galaxy_data in galaxy_data_list:
        if (float(galaxy_data[4]) > criteria):
            spiral_galaxy_list.append(Galaxy(galaxy_data[0:3], "Elliptical"))
    return(spiral_galaxy_list)


def wrapper_download_function(galaxy):
    """
    Wrapper function for downloading image of galaxy. Need for parallel downloading.
    """
    download_code = galaxy.download_image()
    if (download_code == 0):
        print(f"Image of the galaxy {galaxy.ID} is downloaded.")


def download_galaxies_image(directory, galaxy_list, num_process):
    """
    Download images of galaxies in directory (and create it if needed necessary).
    """
    start_directory = os.getcwd()
    if not os.path.exists(directory):
        os.makedirs(directory)
    os.chdir(directory)

    with Pool(num_process) as pool:
        pool.map(wrapper_download_function, galaxy_list)
    os.chdir(start_directory)


def main(args):
    print("Reading data from file...")
    galaxy_data_list = read_galaxies_data('galaxies.csv')
    print("Getting spiral and elliptical galaxies...")
    spiral_galaxy_list = get_spiral_galaxies(galaxy_data_list, 0.95)
    ellipt_galaxy_list = get_ellipt_galaxies(galaxy_data_list, 0.95)
    print("Downloading images...")
    download_galaxies_image("./images/spiral", spiral_galaxy_list[:100], args.number_process)
    download_galaxies_image("./images/ellipt", ellipt_galaxy_list[:100], args.number_process)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('number_process', action='store', type=int, help='input number of processes for downloading')
    args = parser.parse_args()
    if (args.number_process < 1):
        sys.exit("Incorrect number of process: it should be positive integet!")
    main(args)
