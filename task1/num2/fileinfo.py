#!/usr/bin/env python
import argparse


def show_file_info(file_name):
    """
    Print file information: numbers of line, numbers of words and numbers of characters.
    """
    lines_num = 0
    words_num = 0
    chars_num = 0
    with open(file_name, "r") as file_object:
        for line in file_object:
            lines_num += 1
            words = line.strip().split()
            for word in words:
                words_num += 1
                chars_num += len(word) # without spaces
            #chars_num += len(line) # with spaces

    print(f"Number of lines: {lines_num}")
    print(f"Number of words: {words_num}")
    print(f"Number of characters: {chars_num}")


def main(args):
    show_file_info(args.file_name)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('file_name', action='store', help='input file name')
    args = parser.parse_args()
    main(args)
