#!/usr/bin/python
import numpy as np
from time import time
from scipy import odr
from random import random
from statistics import mean, stdev
from matplotlib import pyplot as plt


def bubble_sorted(list):
    for _ in range(len(list) - 1):
        is_sorted = True
        for i in range(len(list) - 1):
            if(list[i] > list[i+1]):
                list[i], list[i+1] = list[i+1], list[i]
                is_sorted = False
        if is_sorted:
            break
    return(list)


def get_time_test(sort_fun, test_list):
    """
    Return running time.
    """
    start = time()
    sort_list = sort_fun(test_list)
    final = time()
    return(final - start)


def get_benchmark(sort_fun, list_size, number_test):
    """
    Return list of running time for given list_size (size of sorting list).
    """
    exe_times = []
    for _ in range(number_test):
        rnd_list = [random() for _ in range(list_size)]
        exe_times.append(get_time_test(sort_fun, rnd_list))
    return(exe_times)


def get_full_benchmark(sort_fun, list_sizes, number_test):
    """
    Return list of mean times and stdev for different sizes of a sorted list.
    """
    mean_times = []
    stdev_times = []
    for list_size in list_sizes:
        exe_times = get_benchmark(sort_fun, list_size, number_test)
        mean_times.append(mean(exe_times))
        stdev_times.append(stdev(exe_times))
    benchmark_data = {'mean': mean_times, 'stdev': stdev_times}
    return(benchmark_data)


def fit_benchmark_data(list_sizes, benchmark_data, fun):
    """
    Fit running times according to the given model.
    """
    data_model = odr.Model(fun)
    data_to_fit = odr.RealData(x=list_sizes, y=benchmark_data['mean'], sy=benchmark_data['stdev'])
    job = odr.ODR(data_to_fit, data_model, beta0=[1., 0.])
    results = job.run()
    return results.beta, results.sd_beta


def plot_becnhmark_data(list_sizes, bubble_data, sorted_data, is_log):
    """
    Plot running times with error bars. If is_log set log scale.
    """
    plt.errorbar(list_sizes, sorted_data['mean'], yerr=sorted_data['stdev'], fmt="ro")
    plt.errorbar(list_sizes, bubble_data['mean'], yerr=bubble_data['stdev'], fmt='bo')
    plt.xlabel('data size')
    plt.ylabel('time (s)')
    if is_log:
        plt.xscale('log')
        plt.yscale('log')


def plot_fitting_fun(list_sizes, beta, fun):
    """
    Plot fitting function.
    """
    x = np.linspace(list_sizes[0], list_sizes[-1], 1000)
    def f(x): return fun(beta, x)
    plt.plot(x, f(x))


def main():
    number_test = 100
    list_sizes = [5*2**i for i in range(8)]
    def bubble_fun(b, x): return b[0] * x**2 + b[1]
    def sorted_fun(b, x): return b[0] * x * np.log(x) + b[1]

    print("Benchmark running...")
    sorted_data = get_full_benchmark(sorted, list_sizes, number_test)
    bubble_data = get_full_benchmark(bubble_sorted, list_sizes, number_test)

    print("Fitting benchmark data...")
    beta_bubble, beta_std_bubble = fit_benchmark_data(list_sizes, bubble_data, bubble_fun)
    beta_sorted, beta_std_sorted = fit_benchmark_data(list_sizes, sorted_data, sorted_fun)
    print(f"Running time bubble: b0 * x**2 + b1")
    print(f"b0 = {beta_bubble[0]} +- {beta_std_bubble[0]}")
    print(f"b1 = {beta_bubble[1]} +- {beta_std_bubble[1]}")
    print(f"Running time sorted: b0 * x * log(x) + b1")
    print(f"b0 = {beta_sorted[0]} +- {beta_std_sorted[0]}")
    print(f"b1 = {beta_sorted[1]} +- {beta_std_sorted[1]}")

    print("Plotting...")
    plot_becnhmark_data(list_sizes, bubble_data, sorted_data, is_log=True)
    plot_fitting_fun(list_sizes, beta_bubble, bubble_fun)
    plot_fitting_fun(list_sizes, beta_sorted, sorted_fun)
    plt.show()
    print("Done!")


if __name__ == '__main__':
    main()
